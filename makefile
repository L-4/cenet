.DEFAULT_GOAL:= run
.PHONY: build clean run

LOVE=love
CLIENT_SRC=src
SERVER_SRC=src/server
LOVE_RELEASE=/home/l4/.luarocks/bin/love-release
CLIENT_BUILD_DIR=build/client
SERVER_BUILD_DIR=build/server
COMMON_BUILD_OPTIONS=--author L4 --email lfourunderscore@gmail.com -v 1 --url 127.0.0.1 --desc "a network library test" --uti com.honest.cenet
BUILD_TARGETS=-M -D -W 64

run:
	$(LOVE) ./src

build:
	mkdir -p ./$(CLIENT_BUILD_DIR)
	$(LOVE_RELEASE) ./$(CLIENT_BUILD_DIR) $(CLIENT_SRC) --title cenet_client $(COMMON_BUILD_OPTIONS) $(BUILD_TARGETS)
	mkdir -p ./$(SERVER_BUILD_DIR)
	$(LOVE_RELEASE) ./$(SERVER_BUILD_DIR) $(SERVER_SRC) --title cenet_server $(COMMON_BUILD_OPTIONS) $(BUILD_TARGETS)

clean:
	$(RM) -r ./build/server/* ||:
	$(RM) -r ./build/client/* ||: