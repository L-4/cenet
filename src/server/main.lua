local cenet = require "common.cenet"
cenet.server = {}
require "common.cenet_packets"
local love = {
	timer = require "love.timer"
}

local host = cenet.host_create("*:2400")

local dt, last_frame, this_frame = 0, love.timer.getTime(), 0

while true do
	this_frame = love.timer.getTime()
	dt = this_frame - last_frame
	last_frame = this_frame

	host:service()

	love.timer.sleep(0.001)
end