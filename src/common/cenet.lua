-- Libraries
local ffi = require "ffi"
local cenet = require "enet"

local host_metatable = debug.getregistry()["enet_host"].__index
local peer_metatable = debug.getregistry()["enet_peer"].__index

-- Variables
local cdefs = {} 					    -- Temporary table for storing the components of the cdef
local packets_by_id = {}      -- ID: p_packet_struct
local packets_by_struct = {}  -- p_packet_struct: ID
local compiled = false		    -- Whether cdefs has been used
local packet_size					    -- Size of largest packet struct (and the size of the packet untion)
local packet_pointer			    -- Pointer to location in memory packets are moved to before being cast
local print_unhandled_packets = true
local struct_checksum

-- More variables for netclass
local net_classes_by_id = {}
local net_classes_by_name = {}
local entities = {}
local unsynced_entities = {}

-- TODO: char[?] fields just don't do anything

-- helper functions

-- The stock assert function evaluates the error message even if condition is false
local function assertx(condition, ...)
	if not condition then error(table.concat({...})) else return condition end
end

-- Adds a formatted string to the cdef table
local function add_cdef(format_string, ...)
	assertx(not compiled, "cdefs have already been compiled")
	cdefs[#cdefs+1] = string.format(format_string, ...)
end

love.filesystem.remove("debug.log")
local function printf(format_string, ...)
	local formatted = (cenet.server and"[SERVER] - "or cenet.client and"[CLIENT] - "or"[UNKNOWN] - ") ..
		string.format(format_string, ...)
	print(formatted)
	assert(love.filesystem.append("debug.log", formatted .. "\n"))
end

-- Used as a default for callbacks that do not exist yet
local anonymous
if print_unhandled_packets then
	anonymous = function(packet, peer)
		printf("Got unhandled packet '%s' (ID: %d) from peer %d",
			packets_by_id[packet.packet_id], packet.packet_id, peer:connect_id())
	end
else
	anonymous = function() end
end

-- Constants, used to make array indexing a bit more readable in the following function
local TYPE = 1
local NAME = 2
local SIZE = 3

function cenet.register_packet(struct_name, struct_data, flag)
	assertx(struct_name, "Missing struct name")
	assertx(struct_data, "Missing struct data")
	assertx(flag == nil or flag == "unsequenced" or flag == "reliable" or flag == "reliable",
		"Illegal flag '", flag ,"'")
	assertx(not packets_by_struct[struct_name], "Struct '", struct_name, "' already exists.")
	assertx(struct_name:find("^[a-zA-Z_][a-zA-Z0-9_]*$"), "Illegal struct name '", struct_name, "'")

	add_cdef("typedef struct {")
	add_cdef("\tint packet_id;")

	for type_num, data in ipairs(struct_data) do
		assertx(data[NAME] ~= "packet_id", "Packet cannot have field 'packet_id'")
		assertx(data[TYPE], "Missing type identifier for type '", type_num, "' of struct '", struct_name, "'")

		if data[SIZE] then
			assertx(tonumber(data[SIZE]) and tonumber(data[SIZE]) > 0,
				"Third argument for struct member must be positive number (size)")
			data[SIZE] = "[" .. data[SIZE] .. "]"
		else
			data[SIZE] = ""
		end

		add_cdef("\t%s %s%s;", data[TYPE], data[NAME], data[SIZE])
	end

	add_cdef("} %s; \n", struct_name)

	packets_by_id[#packets_by_id+1] = {
		struct = struct_name,
		callback = anonymous,
		flag = flag
	}
	packets_by_struct[struct_name] = #packets_by_id
	return #packets_by_id
end

function cenet.register_callback(packet_id, callback)
	assertx(packets_by_id[packet_id].struct, "No such struct")
	packets_by_id[packet_id].callback = callback or anonymous
end

function cenet.call_callback(packet_id, ...)
	return packets_by_id[packet_id].callback(...)
end

function cenet.compile()
	add_cdef("typedef union {") -- Union containing each packet
	add_cdef("\tint packet_id;")
	for _, packet in pairs(packets_by_id) do
		add_cdef("\t%s %s;", packet.struct, packet.struct)
	end

	add_cdef("} packet;\n")

	-- External C functions
	add_cdef("void *malloc(size_t size);")
	add_cdef("void free(void *location);")

	-- Make checksum of packets
	struct_checksum = love.data.hash("md5", table.concat(cdefs))
	printf("Checksum: %s", love.data.encode("string", "base64", struct_checksum))

	-- Finally, "compile" the defitions
	ffi.cdef(table.concat(cdefs, "\n"))

	packet_size = ffi.sizeof("packet")
	packet_pointer = ffi.C.malloc(packet_size) -- Make enough room for the biggest packet possible
end

-- Get enums
function cenet.get_packet_enums() return packets_by_struct end

-- Monkeypatching key parts of enet
local enet_host_create = cenet.host_create
function cenet.host_create(address, peer_count, channel_count, download, upload)
	local host = enet_host_create(address, peer_count, channel_count, download, upload)
	if address then
		cenet.server = {host = host, connections = {}}
	else
		cenet.client = {host = host}
	end
	return host
end

local host_connect = host_metatable.connect
function host_metatable:connect(address, channel_count, data)
	local server = host_connect(self, address, channel_count, data)
	cenet.client.server = server
	cenet.client.connect_id = server:connect_id()
	return server
end

-- Callbacks for connection and disconnection events
function cenet.on_connect(event)    end --luacheck: ignore
function cenet.on_disconnect(event) end --luacheck: ignore

local host_service = host_metatable.service
function host_metatable:service()
	local event = host_service(self)
	while event do
    if event.type == "receive" then
			ffi.copy(packet_pointer, event.data)
			local packet = ffi.cast("packet *", packet_pointer)
			local packet_meta = packets_by_id[packet.packet_id]

			-- printf("got packet: %s", packets_by_id[packet.packet_id].struct)
			packet_meta.callback(packet[packet_meta.struct], event.peer, self)
		elseif event.type == "connect" then
			printf("%i connected. connect_id: %i", event.peer:index(), event.peer:connect_id())
			if cenet.server then
				cenet.server.connections[event.peer:index()] = event.peer:connect_id()
				cenet.call_callback(packets_by_struct.p_request_all_entity_flush, {}, event.peer)
			end
			cenet.on_connect(event)
		elseif event.type == "disconnect" then
			if cenet.server then
				local connect_id = cenet.server.connections[event.peer:index()]
				printf("%s disconnected", event.peer:index())
				for i = 1, #entities do
					if entities[i].alive and entities[i].__owner == connect_id then
						cenet.call_callback(packets_by_struct.p_request_entity_deletion,
							{entity_id = entities[i].__entity_id}, event.peer)
						printf("Entity %i destroyed due to client %d leaving.", i, connect_id)
					end
				end
				cenet.server.connections[event.peer:index()] = nil
			end
			cenet.on_disconnect(event)
		end
		event = host_service(self)
	end
end

local peer_send = peer_metatable.send
function peer_metatable:send(packet_id, ...)
	local packet_meta = packets_by_id[packet_id]
	peer_send(self, ffi.string(ffi.new(packet_meta.struct, packet_id, ...), ffi.sizeof(packet_meta.struct)),
		0, packet_meta.flag)
	-- printf("sent packet: %s", packet_meta.struct)
end

function peer_metatable:send_raw(packet)
	peer_send(self, ffi.string(packet, ffi.sizeof(packets_by_id[packet.packet_id].struct)))
end

local host_broadcast = host_metatable.broadcast
function host_metatable:broadcast(packet_id, ...)
	local packet_meta = packets_by_id[packet_id]
	host_broadcast(self, ffi.string(ffi.new(packet_meta.struct, packet_id, ...), ffi.sizeof(packet_meta.struct)),
		packet_meta.channel, packet_meta.flag)
	-- printf("broadcasted packet: %s", packet_meta.struct)
end

function host_metatable:broadcast_raw(packet)
	host_broadcast(self, ffi.string(packet, ffi.sizeof(packets_by_id[packet.packet_id].struct)))
end

function cenet.deinitialize()
	ffi.C.free(packet_pointer)
end

-- Networked class implementation

-- Relevant packets

-- Packet sent froms server to all clients informing them of new entity
-- with entity ID and entity owner.
cenet.register_packet("p_entity_created", {
	{"unsigned short", "entity_id"},
	{"unsigned long long", "owner"},
	{"unsigned short", "entity_class_id"},
	{"unsigned short", "entity_unique_id"}
}, "reliable")

cenet.register_callback(packets_by_struct.p_entity_created,
function(p_entity_created)
	if cenet.client and cenet.client.connect_id == p_entity_created.owner then
		assertx(unsynced_entities[p_entity_created.entity_unique_id],
			"Tried to sync nonexistant entity with an unique id of ", p_entity_created.entity_unique_id)

		local instance = unsynced_entities[p_entity_created.entity_unique_id]
		instance.__local = nil
		instance.__owner = p_entity_created.owner
		instance.__entity_id = p_entity_created.entity_id
		entities[p_entity_created.entity_id] = instance
		instance:flush()
		unsynced_entities[p_entity_created.entity_unique_id] = nil
	else
		local instance = net_classes_by_id[p_entity_created.entity_class_id]:__instanciate()

		entities[p_entity_created.entity_id] = instance
		instance.__owner = p_entity_created.owner
		instance.__entity_id = p_entity_created.entity_id

		local class_instances = net_classes_by_id[p_entity_created.entity_class_id].instances()

		for i = 1, #class_instances do
			if not class_instances[i].alive then
				class_instances[i] = instance
				return instance
			end
		end
		class_instances[#class_instances+1] = instance
		return instance
	end
end)

-- Packet sent to server to request new entity
cenet.register_packet("p_request_entity", {
	{"unsigned short", "entity_class_id"},
	{"unsigned short", "entity_unique_id"}
}, "reliable")

cenet.register_callback(packets_by_struct.p_request_entity,
function(p_request_entity, peer)
	assertx(cenet.server, "Cannot create entity from client")
	local instance = net_classes_by_id[p_request_entity.entity_class_id]:__instanciate()
	instance.__owner = peer:connect_id()

	local found_id = false
	for i = 1, #entities do
		if not entities[i].alive then
			entities[i] = instance
			instance.__entity_id = i
			found_id = true break
		end
	end
	if not found_id then
		entities[#entities+1] = instance
		instance.__entity_id = #entities
	end

	assertx(instance.__entity_class_id > 0, "instance.__entity_class_id was ", instance.__entity_class_id)
	cenet.server.host:broadcast(packets_by_struct.p_entity_created, instance.__entity_id, instance.__owner,
		instance.__entity_class_id, p_request_entity.entity_unique_id)
end)

cenet.register_packet("p_entity_deleted", {
	{"unsigned short", "entity_id"}
})

cenet.register_callback(packets_by_struct.p_entity_deleted,
function(p_entity_deleted)
	entities[p_entity_deleted.entity_id]:destroy()
end)

cenet.register_packet("p_request_entity_deletion", {
	{"unsigned short", "entity_id"}
}, "reliable")

cenet.register_callback(packets_by_struct.p_request_entity_deletion,
function(p_request_entity_deletion, peer)
	assertx(cenet.server, "Entity deletion packet can only be handled by server")
	if cenet.server.connections[peer:index()] == entities[p_request_entity_deletion.entity_id].__owner then
		cenet.server.host:broadcast(packets_by_struct.p_entity_deleted, p_request_entity_deletion.entity_id)
		cenet.call_callback(packets_by_struct.p_entity_deleted, {entity_id = p_request_entity_deletion.entity_id})
		-- TODO: make cenet.call_callback not required when broadcasting from server
	end
end)

cenet.register_packet("p_request_all_entity_flush", {}, "reliable")

cenet.register_callback(packets_by_struct.p_request_all_entity_flush,
function(p_request_all_entity_flush, peer) -- luacheck: ignore
	assertx(cenet.server, "Server only")

	local instance
	for i = 1, #entities do
		if entities[i].alive then
			instance = entities[i]
			assertx(instance.__entity_class_id > 0, "instance.__entity_class_id was ", instance.__entity_class_id)
			peer:send(packets_by_struct.p_entity_created, instance.__entity_id, instance.__owner,
				instance.__entity_class_id)
			peer:send(instance:__get_flush_packet())
		end
	end
end)

local unpack = unpack or table.unpack

local networked_class_metatable = {
	__instanciate = function(self, ...) -- TODO: move following three methods into netclass metatable
		local instance = setmetatable({alive=true}, self)
		instance:populate(...)

		return instance
	end,
	owned_by_client = function(self)
		return self.__owner == cenet.client.connect_id
	end,
	destroy = function(self)
		self.alive = false
	end
}

function cenet.networked_class(name, flush_data, update_data)
	assertx(not compiled, "Cannot create new networked class after cdefs are compiled")
	assertx(not net_classes_by_name[name], "Networked class '", name, "' already defined.")

	table.insert(flush_data, 1, {"unsigned short", "entity_id"})
	if update_data then
		table.insert(update_data, 1, {"unsigned short", "entity_id"})
	end

	local instances = {}

	local net_class = setmetatable({
		__entity_class_id = #net_classes_by_id + 1,
		__instanciate = function(self, ...) -- TODO: move following three methods into netclass metatable
			local instance = setmetatable({alive=true}, self)
			instance:populate(...)

			return instance
		end,
		owned_by_client = function(self)
			return self.__owner == cenet.client.connect_id
		end,
		destroy = function(self)
			self.alive = false
		end,
		-- flush() and update() are essentially the same funciton with different parameters, and more
		-- similar functions could be useful in the future. One could create a JIT macro and a) be able
		-- to use as many of these functions as you want, and b) not have to include them by default.
		-- This, however, would reqiore a rewrite of the way entities are synced at creation.
		-- Maybe have a few "keywords" that are used for flushing and updating, optionally?
		__flush_data = flush_data,
		__flush_struct_id = cenet.register_packet("nc_flush_" .. name, flush_data, "reliable"),
		__get_flush_packet = function(self)
			local new_flush_data = {}
			for i = 2, #self.__flush_data do -- TODO: create temp table as to not have to iterate from 2
				new_flush_data[i-1] = self[self.__flush_data[i][NAME]]
			end
			return self.__flush_struct_id, self.__entity_id, unpack(new_flush_data)
		end,
		flush = function(self)
			if self.__local then return end
			cenet.client.server:send(self:__get_flush_packet())
		end,
		__update_data = update_data, -- TODO: both flush and update could be JIT compiled
		__update_struct_id = update_data and cenet.register_packet("nc_update_" .. name,
			update_data, "unsequenced") or -1,
		update = update_data and function(self)
			if self.__local then return end
			local new_update_data = {}
			for i = 2, #self.__update_data do
				new_update_data[i-1] = self[self.__update_data[i][NAME]]
			end
			cenet.client.server:send(self.__update_struct_id, self.__entity_id, unpack(new_update_data))
		end or function() end,
		instances = function() return instances end -- TODO: this could just be a member of the class.
	}, {
		__call = function(self, ...)
			-- Client calls __call(some, entity, params)
			-- __call sends entity request to server with unique entity ID (only unique per client,
			--		client connection id + unique ID guaranteed to be unique)
			-- __call returns instance of entity with params set, and keeps a reference to said instance,
			--		and sets __local to true
			-- Some time passes. Instance uses __local to decide not to run any netcode
			-- Client gets entity creation packet, sees that it is the owner, matches it to the entity unique id
			-- Client fixes the __owner and __entity_id fields of said instance in place,
			--		and moves it into the list of entities
			-- Client forces a __flush on the entity to update data potentially changed since it's creation
			-- Client detects an entity flush sent for said entity, and now sets it's __local flag to nil
			assertx(cenet.client, "class creation request must be run from client")
			-- TODO: create class instance from server

			local id = 0
			repeat id = id + 1 until not unsynced_entities[id]

			cenet.client.server:send(packets_by_struct.p_request_entity, self.__entity_class_id, id)

			local instance = self:__instanciate(...)
			instance.__local = true

			unsynced_entities[id] = instance
			return instance
			-- What should happen™:
		end
	})
	net_class.__index = net_class

	cenet.register_callback(net_class.__flush_struct_id,
	function(flush_struct)
		if cenet.server then
			cenet.server.host:broadcast_raw(flush_struct)
		end

		local instance = entities[flush_struct.entity_id]
		if not instance then printf("Got update for nonexistant entity %s", flush_struct.entity_id) return end
		if cenet.client and cenet.client.connect_id == instance.__owner then return end

		for i = 2, #instance.__flush_data do
			instance[instance.__flush_data[i][NAME]] = flush_struct[instance.__flush_data[i][NAME]]
		end
	end)

	if update_data then
		cenet.register_callback(net_class.__update_struct_id,
		function(update_struct)
			if cenet.server then
				cenet.server.host:broadcast_raw(update_struct)
			end

			local instance = entities[update_struct.entity_id]
			if not instance then printf("Got update for nonexistant entity %s", update_struct.entity_id) return end
			if cenet.client and cenet.client.connect_id == instance.__owner then return end
			for i = 2, #instance.__update_data do
				instance[instance.__update_data[i][NAME]] = update_struct[instance.__update_data[i][NAME]]
			end
		end)
	end
	net_classes_by_id[#net_classes_by_id+1] = net_class
	net_classes_by_name[name] = #net_classes_by_id

	return net_class
end

-- How to seperate types of entities?
-- Three bit flag for simulation location?
-- Simulated on |Bullets|Enemies|
-- Owner        |       |       |
-- Server       |   X   |   X   |
-- Clients      |       |       |

return cenet