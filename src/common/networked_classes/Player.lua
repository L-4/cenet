local cenet = require "common.cenet"
local ffi = require "ffi"

local Player = cenet.networked_class("Player", {
	{"float", "x"}, -- Flush struct. For flushing all data between all clients, reliably
	{"float", "y"},
	{"char", "name", 32}
}, {
	{"float", "x"}, -- Update struct (optional), updates each frame (or at other intervals) using unreliable packets
	{"float", "y"}
})

local function printf(format_string, ...)
	local formatted = (cenet.server and"[SERVER] - "or cenet.client and"[CLIENT] - "or"[UNKNOWN] - ") ..
		string.format(format_string, ...)
	print(formatted)
	assert(love.filesystem.append("debug.log", formatted .. "\n"))
end

local firstnames = {"bob", "jim", "joe", "rolan"}

local lastnames  = {"bobsson", "cheeseburger"}

function Player:populate(x, y, name)
	self.x, self.y = x or 0, y or 0
	self.name = name or string.format("%s %s", firstnames[math.random(#firstnames)], lastnames[math.random(#lastnames)])
end

-- Stuff below this line has nothing to do with the library
if cenet.client then

function Player:draw()
	printf("playe")
	love.graphics.printf(ffi.string(self.name), self.x - 50, self.y - 50, 100, "center")
	love.graphics.circle("fill", self.x, self.y, 15)
end

local players = Player.instances()

function Player.draw_all()
	for i = 1, #players do
		if players[i].alive then
			players[i]:draw()
		end
	end
end

local isDown = love.keyboard.isDown

function Player:move(dt)
	self.x = self.x + (isDown"a" and -1 or isDown"d" and 1 or 0) * 500 * dt
	self.y = self.y + (isDown"w" and -1 or isDown"s" and 1 or 0) * 500 * dt

	self:update(dt) -- (except for this, for now)
end

end

return Player