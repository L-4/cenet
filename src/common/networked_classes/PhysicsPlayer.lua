local cenet = require "common.cenet"

local Player = cenet.networked_class("Player", {
	{"float", "x"}, -- Flush struct. For flushing all data between all clients, reliably
	{"float", "y"}
}, {
	{"float", "x"}, -- Update struct (optional), updates each frame (or at other intervals) using unreliable packets
	{"float", "y"}
})

function Player:populate()
	self.x, self.y = 0, 0
end

-- Stuff below this line has nothing to do with the library
if cenet.client then

function Player:draw()
	love.graphics.circle("fill", self.x, self.y, 15)
end

local players = Player.instances()

function Player.draw_all()
	for i = 1, #players do
		if players[i].alive then
			players[i]:draw()
		end
	end
end

local isDown = love.keyboard.isDown

function Player:move(dt)
	self.x = self.x + (isDown"a" and -1 or isDown"d" and 1 or 0) * 500 * dt
	self.y = self.y + (isDown"w" and -1 or isDown"s" and 1 or 0) * 500 * dt

	self:update(dt) -- (except for this, for now)
end

end

return Player