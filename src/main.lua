-- require "common.lib.errorhandler"
local cenet = require "common.cenet"
cenet.client = {}
require "common.cenet_packets"
local argparse = require "common.lib.argparse"

local Player = require "common.networked_classes.Player"

local player
function love.draw()
	Player.draw_all()
	if player then player:draw() end
end

local host = cenet.host_create()
local server
function love.load(arg)
	local parser = argparse()
	parser:option "--port"
		:default "2400"
	parser:option "--ip"
		:default "localhost"
	parser:flag "--noserver"

	local arguments = parser:parse(arg)

	if not arguments.noserver then
		love.window.setPosition(0, 0)
		love.thread.newThread("server/main.lua"):start()
	else
		love.window.setPosition(1000, 0)
	end

	server = host:connect(arguments.ip .. ":" .. arguments.port)
end

function love.update(dt)
	if server then
		host:service()
	end
	if player then
		player:move(dt)
	end
end

function love.keypressed(key)
	if key == "space" then
		player = Player(100, 200, "ya boi")
	elseif key == "escape" then
		love.event.quit()
	end
end

local disconnecting = false

function love.quit()
	print("DISCONNECTING")
	if not disconnecting then
		cenet.client.server:disconnect()
		cenet.deinitialize()
		disconnecting = true
	end
	repeat
		love.timer.sleep(0.001)
		cenet.client.host:service()
	until cenet.client.server:state() == "disconnected"
end